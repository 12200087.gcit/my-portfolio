const logotext = "Tandin Jamtsho";
const meta = {
    title: "teejeey",
    description: "I’m Tandin Jamtsho Designer _ Full stack devloper,currently studying in Gyalpozhing College of Information Technology",
};

const introdata = {
    title: "I’m Tandin Jamtsho",
    animated: {
        first: "I love coding",
        second: "I code cool websites",
        third: "I develop mobile apps",
    },
    description: "Hey, I'm Tandin Jamtsho – a passionate designer and coder. With a heart set on creating captivating digital experiences, I blend creativity and technology to bring ideas to life. Based in Gyalpozhing College of Information Technology, Kabjisa, Thimphu, my journey is a constant exploration of design aesthetics and coding possibilities. Let's connect and embark on a journey of innovation together.",
    your_img_url: "https://yourimageshare.com/ib/XdXdtSYXb5.webp", // Home page profile
};

const dataabout = {
    title: "About My-self",
    aboutme: "Hey, I'm Tandin Jamtsho – a passionate designer and coder. With a heart set on creating captivating digital experiences, I blend creativity and technology to bring ideas to life. Based in Gyalpozhing College of Information Technology, Kabjisa, Thimphu, my journey is a constant exploration of design aesthetics and coding possibilities. Let's connect and embark on a journey of innovation together.",
};
const worktimeline = [
    {
        jobtitle: "1) Best Project - Annual GCIT project showcase - Parking.bt ",
        where: "GCIT,Kabjisa, Thimphu",
        date: "3rd June, 2024",
    },
    {
        jobtitle: "2) Second Runners-up in TECH-HACK 2024 (DigiOps)",
        where: "IT Park, Thimphu - GovTech&NDI",
        date: "8th-10th, March, 2024",
    },
    
    {
        jobtitle: "3) Second Runners-up in TECH-HACK 2022 (Brother Squad)",
        where: "Jigme Namgyal Wangchuck SuperFabLab, Thimphu - DHI&GCIT",
        date: "5th, December, 2022",
    },
    {
        jobtitle: "4) Winner of PRJ-202 Exibition - GCIT Prayer Book",
        where: "GCIT, Gyalpozhing, Mongar - GCIT Project Showcase",
        date: "2022",
    },
    {
        jobtitle: "5) Recognition of being well content creater of Short Film Competition",
        where: "GCIT, Gyalpozhing, Mongar - World Saving Day, 2021",
        date: "2021",
    },
    {
        jobtitle: "6) Participate in Cluster Level Science and Technology Exhibition",
        where: "Jakar Higher Secondary School - National STEM Olympaid",
        date: "22nd, April, 2017",
    },
];

const skills = [{
        name: "Python",
        value: 90,
    },
    {
        name: "Javascript",
        value: 80,
    },
    {
        name: "React",
        value: 90,
    },

    {
        name: "React Native",
        value: 90,
    },
    {
        name: "Design - (UX/UI)",
        value: 85,
    },
];

const services = [{
        title: "UI & UX Design",
        description: "Through our expertise, we produce user-friendly designs that enhance aesthetics and facilitate seamless navigation in your digital environment. Our priority is to make your website or app visually appealing and easy to use, resulting in a captivating and effortless experience for your audience.",
    },
    {
        title: "Mobile Apps",
        description: "Our services lies in turning your ideas into interactive and fully operational mobile apps that cater precisely to our audience's requirements. From concept to creation, we ensure your app meets the highest standards of functionality and design.",
    },
    {
        title: "Web Development",
        description: "We provide specializes in building user-friendly websites that showcase your brand and engage your visitors effectively. Let us help you create a strong digital footprint that leaves a lasting impression.",
    },
];

const dataportfolio = [
    {
        img: "https://yourimageshare.com/ib/kV7IcUslrH.webp",
        description: "Parking.bt - (Ongoing), Parking.bt revolutionizes Bhutan's parking management with a mobile and web app. It replaces manual fee collection to reduce conflicts, prevent revenue leakage, and improve efficiency. ",
        link: "#",
        live :'#'
    },
    {
        img: "https://yourimageshare.com/ib/Pl3V9nvnrY.webp",
        description: "Lakhor: Convenience at Your Fingertips enhances travel in Bhutan by connecting passengers directly to drivers via geolocation tracking. It offers standardized fares and a safe, reliable solution for a better travel experience. With user-friendly features, real-time updates, and efficient route planning, Lakhor ensures that both residents and visitors enjoy seamless, cost-effective, and secure transportation across the country.",
        link: "#",
        live :'#'
    },

    {
        img: "https://yourimageshare.com/ib/vbBtG0Cja5.webp",
        description: "GCIT Mess Procurement System transforms college mess procurement through an integrated platform, providing real-time data access, streamlined inventory management, and cost-effective solutions to enhance decision-making and reduce errors.",
        link: "https://gitlab.com/2023its1g7.gcit/mess-procurement-system",
        live :'https://www.youtube.com/watch?v=6Wm2m86F6Wo'
    },

    {
        img: "https://yourimageshare.com/ib/uzO0dxujaJ.webp",
        description: "Druk Calculator: Addressing the linguistic and numerical requirements of Bhutan, our user-friendly tool excels in both Dzongkha and English. Designed for students, professionals, and the monastic community, it ensures precise calculations throughout Bhutan.",
        link: "https://gitlab.com/12200087.gcit/druk_calculator/-/tree/main/Dzongkhacalculator?ref_type=heads",
        live :'#'
    },

    {
        img: "https://yourimageshare.com/ib/K67sAzock7.webp",
        description: "Fashion Bhutan aims to simplify online shopping by providing a user-friendly platform for customers.  offering a convenient and efficient way for users to browse, select, and purchase a variety of products. Providing enjoyable shopping experience, enhance the convenience and satisfaction of online shoppers.",
        link: "https://gitlab.com/12200087.gcit/druk_calculator/-/tree/main/Dzongkhacalculator?ref_type=heads",
        live :'https://www.figma.com/proto/YVGYwbNjTLUk0agUxAfNpR/Prototype?type=design&node-id=3-3&t=29ctSxL6NJs6HqMJ-0&scaling=min-zoom&page-id=0%3A1&starting-point-node-id=3%3A3'
    },

    // {
    //     img: "https://yourimageshare.com/ib/qmR8jxYeNT.webp", // Self
    //     description: "Welcome Viewers- འབྱོན་པ་ལེགས་སོ། ",
    //     link: "#",
    //     live :'#'
    // },

    {
        img: "https://yourimageshare.com/ib/rIjZfNuwUI.webp", // Drung-Tshab
        description: "Drung-Tshab system combines machine learning and chatbot technology to predict diseases based on symptoms, offering preliminary diagnoses and treatment suggestions before doctor consultations. System predict four major diseases and Dr-Bot predicts 41 minor disease. Dr-Bot is accessible via Telegram.",
        link: "https://gitlab.com/12200087.gcit/drung_tshab",
        live :'https://drungtshab.onrender.com/'
    },
    {
        img: "https://yourimageshare.com/ib/FR6uSkU2ZR.webp", //GCIT Prayer Book
        description: "GCIT Prayer Book app, versatile cross-platform to digitizes prayer recitals and attendance. Responding to challenges with existing prayer books and attendance tracking, it offers two sections: Prayers for all users, and Attendance with councillor access. Aiming for efficiency and enhancement, this innovation promises positive change at GCIT.",
        link: "#",
        live :'https://drungtshab.onrender.com/'
    },
    {
        img: "https://yourimageshare.com/ib/Zc3C3ycx2t.webp", // Layog Seldra
        description: "To address the modern job market faces challenges,  Layog Seldra will connect job seekers and employers. It streamlines job hunting and posting, fostering fairness, economic growth through an accessible platform.",
        link: "https://gitlab.com/12200087.gcit/layog_seldra",
        live :'https://www.youtube.com/watch?v=6Wm2m86F6Wo'
    },
        {
        img: "https://yourimageshare.com/ib/aXFQHJCc1t.webp", // Druk Typing Game
        description: "Druk Typing Game is a user-friendly app designed to enhance Dzongkha typing skills and promote our national language. With its 'basic mode' and 'typing mode,' the app is engaging and informative, helping users improving Dzongkha typing skills, learn Dzongkha letters, improve spelling, and enhance pronunciation.",
        link: "https://gitlab.com/d6761/druk-keyboard",
        live :'https://www.youtube.com/watch?v=JUUW_fm-JVI'
        
    },
    
    {
        img: "https://yourimageshare.com/ib/1HdqkwUJiX.webp", // Personal Portfolio
        description: "Personal Portfolio - I’m Tandin Jamtsho currently pursuing BSc. Information Technology at Gyalpozhing College of Information Technology, Kabjisa, Thimphu, Bhutan.",
        link: "https://gitlab.com/12200087.gcit/my-portfolio",
        live :"#"
    },

    

]
const datadesign = [
    {
        img: "https://yourimageshare.com/ib/gN5mazLNeu.webp", // Apartment Finder Design
        description: 'Introducing the "Apartment Finder" design concept - revolutionary approach to find your dream apartment through our innovative app, guaranteeing a spam-free social media housing search. Your ideal home is just a tap away with our user-friendly app design.',
        // link: "#",
        live :'https://www.figma.com/proto/kl1DSijnfAJEIfgMGOiny8/Untitled?type=design&node-id=1-476&t=ybpEE8DXvx0nESNu-0&scaling=scale-down&page-id=1%3A475&starting-point-node-id=1%3A591'
    },
    {
        img: "https://yourimageshare.com/ib/rIjZfNuwUI.webp", // Drung-Tshab design 
        description: 'Presenting the "Drung-Tshab website" design concept – A only design concept for Drung-Tshab website. Website already in live with different design available in project category',
        // link: "#",
        live :'https://www.figma.com/proto/YUqWmhhZd4EVzesze34BCI/prototype?type=design&node-id=176-3566&t=YFfv3NGtQYhXgFZv-0&scaling=scale-down-width&page-id=0%3A1&starting-point-node-id=24%3A2&hide-ui=1'
    },
    {
        img: "https://yourimageshare.com/ib/uHLnOYDyJz.webp", // Donate for Life Design
        description: 'Introducing the "Blood donation"  design concept - a groundbreaking solution for all your blood donation needs. You can seamlessly donate, receive, and request blood, creating a efficient way to address critical blood requirements. With real-time updates on blood availability, we ensure that urgent needs are met promptly.',
        // link: "#",
        live :'https://www.figma.com/proto/9atRe3lBygY33djWYzYegC/Untitled?type=design&node-id=21-459&t=ybpEE8DXvx0nESNu-0&scaling=scale-down&page-id=0%3A1&starting-point-node-id=19%3A354'
    },
    {
        img: "https://yourimageshare.com/ib/fGa4rrLaAR.webp", // GCIT Revamp Design
        description: 'Presenting the "GCIT Revamp Website" design concept – a new revamp website  design for Gyalpozhing College of Information Technology in Kabjisa, Thimphu.',
        // link: "#",
        live :'https://www.figma.com/proto/uRPJADXoo1H3LnFUPF0kko/CA3?type=design&node-id=21-697&t=zOTNvzMXte6GZnpe-0&scaling=scale-down-width&page-id=0%3A1&starting-point-node-id=288%3A709'
    },
   
    // {
    //     img: "https://picsum.photos/400/700/?grayscale",
    //     description: "The wisdom of life consists in the elimination of non-essentials.",
    //     link: "#",
    //     live :'https://drungtshab.onrender.com/'
    // },
];

const dataphotography = [

    // {
    //     img: "https://yourimageshare.com/ib/qmR8jxYeNT.webp", // Self
    //     description: "Welcome Viewers- འབྱོན་པ་ལེགས་སོ། ",
    //     link: "#",
    //     live :'#'
    // },

    // {
    //     img: "https://picsum.photos/400/600/?grayscale",
    //     description: "The wisdom of life consists in the elimination of non-essentials.",
    //     link: "#",
    //     live :'https://drungtshab.onrender.com/'
    // },

]

const contactConfig = {
    YOUR_EMAIL: "12200087.gcit@rub.edu.bt",
    YOUR_FONE: "+975 77858660 / 17400063",
    description: "Feel free to reach out to me with any inquiries, collaborations, or just to say hello! I'm excited to connect and discuss exciting possibilities. Looking forward to hearing from you!",
    contactaddress: "Gyalpozhing College of Informtaion Technology",
    // creat an emailjs.com account 
    // check out this tutorial https://www.emailjs.com/docs/examples/reactjs/
    YOUR_SERVICE_ID: "contactformid",
    YOUR_TEMPLATE_ID: "emailtemplateid",
    YOUR_USER_ID: "Y2Fux1eR2GHcTmr81",
};

const socialprofils = {
    whatsapp: "https://wa.me/97577858660",
    facebook: "https://www.facebook.com/jamtsho.tandin.750",
    linkedin: "https://www.linkedin.com/in/tandin-jamtsho/",
    twitter: "https://instagram.com/tee_jeey_67?utm_source=qr&igshid=NGExMmI2YTkyZg%3D%3D",
};
export {
    meta,
    dataabout,
    dataportfolio,
    worktimeline,
    skills,
    services,
    introdata,
    contactConfig,
    socialprofils,
    logotext,
    datadesign,
    dataphotography
};