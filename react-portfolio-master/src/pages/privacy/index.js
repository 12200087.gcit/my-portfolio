// import React from 'react';

// export const PrivacyPolicy = () => {
//     return (
//         <div style={styles.container}>
//             <h2>Privacy Policy - Parking.bt</h2>
//             {/* <p>Effective Date: [Insert Date]</p> */}

//             <h3>1. Introduction</h3>
//             <p>Welcome to parking.bt. Our mission is to transform parking management in Bhutan through our mobile and web application solution. This privacy policy explains how we collect, use, and share your personal information.</p>

//             <h3>2. Information We Collect</h3>
//             <p>We collect personal information that you provide to us when using our applications or contacting us. This may include your name, contact details, and payment information.</p>

//             <h3>3. How We Use Your Information</h3>
//             <p>Your information is used to improve our services, facilitate parking management, process payments, and communicate with you about updates and changes to our services.</p>

//             <h3>4. Sharing Your Information</h3>
//             <p>We do not share your personal information with third parties except as necessary to comply with legal obligations or to protect our rights.</p>

//             <h3>5. Data Security</h3>
//             <p>We implement security measures to protect your information. However, no method of transmission over the internet or electronic storage is 100% secure.</p>

//             <h3>6. Your Rights</h3>
//             <p>You have the right to access, correct, or delete your personal information. Contact us to exercise these rights.</p>

//             <h3>7. Changes to This Policy</h3>
//             <p>We may update this privacy policy from time to time. Any changes will be posted on this page with an updated effective date.</p>

//             <h3>.</h3>
//             {/* <h3>8. Contact Us</h3>
//             <p>If you have any questions about this policy, please contact us at:</p>
//             <p>Email: [Your Email]</p>
//             <p>Address: [Your Address]</p> */}
//         </div>
//     );
// }

// const styles = {
//     container: {
//         width: '80%',
//         margin: '20px auto',
//         fontFamily: 'Arial, sans-serif',
//         lineHeight: '1.6',
//         color: '#333',
//     }
// };

// // export default PrivacyPolicy;
