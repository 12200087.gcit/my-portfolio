import React, { useState } from "react";
import "./style.css";
import { Helmet, HelmetProvider } from "react-helmet-async";
import { Container, Row, Col, Tab, Tabs } from "react-bootstrap"; // Import Tabs
import { dataportfolio, meta, datadesign, dataphotography } from "../../content_option";

export const Portfolio = () => {
  const [selectedCategory, setSelectedCategory] = useState("Portfolio");

  return (
    <HelmetProvider>
      <Container className="About-header">
        <Helmet>
          {/* ... your existing Helmet content */}
        </Helmet>
        <Row className="mb-3 mt-3 pt-md-3">
          <Col lg="8">
            <h1 className="display-4 mb-4"> Portfolio </h1>
            <hr className="t_border my-4 ml-0 text-left" />
          </Col>
        </Row>
        <Tabs
          id="category-tabs"
          activeKey={selectedCategory}
          onSelect={(category) => setSelectedCategory(category)}
        >
          <Tab eventKey="Portfolio" title="Portfolio">
            <div className="mb-5 po_items_ho">
              {dataportfolio.map((data, i) => (
                <div key={i} className="po_item">
                  <img src={data.img} alt="" />
                  <div className="content">
                    <p>{data.description}</p>
                    <a href={data.link}>view Project</a>
                    <a href={data.live}>Live Demo</a>
                  </div>
                </div>
              ))}
            </div>
          </Tab>
          <Tab eventKey="Design" title="Design">

          {/* <div className="design-content">
            <h1>Design-Content</h1>
          </div> */}

          <div className="mb-5 po_items_ho">
              {datadesign.map((data, i) => (
                <div key={i} className="po_item">
                  <img src={data.img} alt="" />
                  <div className="content">
                    <p>{data.description}</p>
                    <a href={data.live}>View Figma Design</a>
                    {/* <a href={data.live}>Live Demo</a> */}
                  </div>
                </div>
              ))}
            </div>
           
          </Tab>
          <Tab eventKey="Photograph" title="Photography">
            <div className="photograph-content m-3">
            <h1>Content Coming Soon</h1>
            </div>
            <div className="mb-5 po_items_ho">
              {dataphotography.map((data, i) => (
                <div key={i} className="po_item">
                  <img src={data.img} alt="" />
                  <div className="content">
                    <p>{data.description}</p>
                    {/* <a href={data.live}>View Figma Design</a> */}
                    {/* <a href={data.live}>Live Demo</a> */}
                  </div>
                </div>
              ))}
            </div>
          </Tab>
        </Tabs>
      </Container>
    </HelmetProvider>
  );
};


// import React, { useState } from "react";
// import "./style.css";
// import { Helmet, HelmetProvider } from "react-helmet-async";
// import { Container, Row, Col, DropdownButton, Dropdown } from "react-bootstrap";
// import { dataportfolio, meta } from "../../content_option";

// export const Portfolio = () => {
//   const [selectedCategory, setSelectedCategory] = useState("Portfolio");

//   const handleCategoryChange = (category) => {
//     setSelectedCategory(category);
//   };

//   return (
//     <HelmetProvider>
//       <Container className="About-header">
//         <Helmet>
//           <meta charSet="utf-8" />
//           <title> Portfolio | {meta.title} </title>{" "}
//           <meta name="description" content={meta.description} />
//         </Helmet>
//         <Row className="mb-5 mt-3 pt-md-3">
//           <Col lg="8">
//             <h1 className="display-4 mb-4"> Portfolio </h1>
//             <hr className="t_border my-4 ml-0 text-left" />
//             <DropdownButton
//               id="category-dropdown"
//               title="Select Category"
//               onSelect={handleCategoryChange}
//             >
//               <Dropdown.Item eventKey="Portfolio">Portfolio</Dropdown.Item>
//               <Dropdown.Item eventKey="Design">Design</Dropdown.Item>
//               <Dropdown.Item eventKey="Photograph">Photograph</Dropdown.Item>
//             </DropdownButton>
//           </Col>
//         </Row>
//         {selectedCategory === "Portfolio" && (
//           <div className="mb-5 po_items_ho">
//             {dataportfolio.map((data, i) => (
//               <div key={i} className="po_item">
//                 <img src={data.img} alt="" />
//                 <div className="content">
//                   <p>{data.description}</p>
//                   <a href={data.link}>view Project</a>
//                   <a href={data.live}>Live Demo</a>
//                 </div>
//               </div>
//             ))}
//           </div>
//         )}
//         {selectedCategory === "Design" && (
//           <div className="design-content">
//             <h1>design-content</h1>
//           </div>
//         )}
//         {selectedCategory === "Photograph" && (
//           <div className="photograph-content">
//             <h1>photograph-content</h1>
//           </div>
//         )}
//       </Container>
//     </HelmetProvider>
//   );
// };


// import React from "react";
// import "./style.css";
// import { Helmet, HelmetProvider } from "react-helmet-async";
// import { Container, Row, Col } from "react-bootstrap";
// import { dataportfolio, meta } from "../../content_option";

// export const Portfolio = () => {
//   return (
//     <HelmetProvider>
//       <Container className="About-header">
//         <Helmet>
//           <meta charSet="utf-8" />
//           <title> Portfolio | {meta.title} </title>{" "}
//           <meta name="description" content={meta.description} />
//         </Helmet>
//         <Row className="mb-5 mt-3 pt-md-3">
//           <Col lg="8">
//             <h1 className="display-4 mb-4"> Portfolio </h1>{" "}
//             <hr className="t_border my-4 ml-0 text-left" />
//           </Col>
//         </Row>
//         <div className="mb-5 po_items_ho">
//           {dataportfolio.map((data, i) => {
//             return (
//               <div key={i} className="po_item">
//                 <img src={data.img} alt="" />
//                 <div className="content">
//                   <p>{data.description}</p>
//                   <a href={data.link}>view Project</a>
//                   <a href={data.live}>Live Demo</a>
//                 </div>
//               </div>
//             );
//           })}
//         </div>
//       </Container>
//     </HelmetProvider>
//   );
// };
